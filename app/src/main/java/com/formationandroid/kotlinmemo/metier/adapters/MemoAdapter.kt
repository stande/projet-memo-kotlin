package com.formationandroid.kotlinmemo.metier.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.kotlinmemo.R
import com.formationandroid.kotlinmemo.metier.constantes.Constant
import com.formationandroid.kotlinmemo.metier.dto.Memo
import com.formationandroid.kotlinmemo.views.MemoDetail
import com.formationandroid.kotlinmemo.views.fragments.MemoDetailFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.memo_item_liste.view.*
import java.util.*

class MemoAdapter(
    private var listeMemos: MutableList<Memo>,
    private var activity: AppCompatActivity
) : RecyclerView.Adapter<MemoAdapter.MemoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val viewMemo =
            LayoutInflater.from(parent.context).inflate(R.layout.memo_item_liste, parent, false)
        return MemoViewHolder(viewMemo)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        holder.textViewIntitule.text = listeMemos[position].intitule
    }

    override fun getItemCount(): Int {
        return listeMemos.size
    }

    // Appelé à chaque changement de position, pendant un déplacement.
    fun onItemMove(positionDebut: Int, positionFin: Int): Boolean {
        Collections.swap(listeMemos, positionDebut, positionFin)
        notifyItemMoved(positionDebut, positionFin)
        return true
    }

    // Appelé une fois à la suppression.
    fun onItemDismiss(position: Int) {
        if (position > -1) {
            // il existe surement une meilleure maniere pour appeler le DAO ...
            //AppDatabaseHelper.getDatabase(activity).memoDAO().delete(listeMemos[position])
            listeMemos.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    /**
     * Ajout d'un mémo à la liste.
     * @param memo Mémo
     */
    fun addMemo(memo: Memo) {
        listeMemos.add(0, memo)
        notifyItemInserted(0)
    }

    /**
     * Récupération d'un memo par son id de liste
     */
    fun getMemoById(position: Int): Memo {
        return listeMemos[position]
    }

    /**
     * ViewHolder.
     */
    inner class MemoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Vue intitulé mémo :
        var textViewIntitule: TextView = itemView.intitule_memo

        /**
         * Constructeur.
         */
        init {
            // listener :
            textViewIntitule.setOnClickListener {

                val memo: Memo = listeMemos[adapterPosition]

                launchInterface(activity, memo.toString())

            }
        }

        private fun launchInterface(
            context: Context,
            memoToString: String
        ) {
            val displayMetrics = context.resources.displayMetrics
            //val dpHeight = displayMetrics.heightPixels / displayMetrics.density
            val dpWidth = displayMetrics.widthPixels / displayMetrics.density
            if (activity.conteneur_memo_detail == null && dpWidth < 600F) {
                val intent = Intent(context, MemoDetail::class.java)
                intent.putExtra(Constant.PARAM_MEMO_DETAIL, memoToString)
                context.startActivity(intent)
            } else {
                // fragment :
                val fragment =
                    MemoDetailFragment()
                // Parametre
                val bundle = Bundle()
                bundle.putString(Constant.PARAM_MEMO_DETAIL, memoToString)
                fragment.arguments = bundle
                // fragment manager :
                val fragmentManager =
                    activity.supportFragmentManager
                // transaction :
                val fragmentTransaction =
                    fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.conteneur_memo_detail,
                    fragment,
                    Constant.FRAGMENT_MEMO_DETAIL
                )
                fragmentTransaction.commit()
            }
        }
    }
}