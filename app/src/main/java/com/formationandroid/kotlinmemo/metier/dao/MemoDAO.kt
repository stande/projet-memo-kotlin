package com.formationandroid.kotlinmemo.metier.dao

import androidx.room.*
import com.formationandroid.kotlinmemo.metier.dto.Memo

@Dao
interface MemoDAO {

    @Query("SELECT * FROM memo ORDER BY memoId DESC")
    fun getAll(): MutableList<Memo>

    @Query("SELECT * FROM memo WHERE memoId = :memoId")
    fun findById(memoId: Long): Memo

    @Query("SELECT * FROM memo WHERE intitule LIKE :intitule LIMIT 1")
    fun findByIntitule(intitule: String): Memo

    @Insert
    fun insert(vararg memo: Memo)

    @Update
    fun update(vararg memo: Memo)

    @Delete
    fun delete(vararg memo: Memo)

}