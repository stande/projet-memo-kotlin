package com.formationandroid.kotlinmemo.metier.helpers

import android.content.Context
import androidx.room.Room
import com.formationandroid.kotlinmemo.metier.database.AppDatabase

class AppDatabaseHelper(
    context: Context, var database: AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "memo.db")
            .allowMainThreadQueries()
            .build()
) {

    companion object {
        private var databaseHelper: AppDatabaseHelper? = null

        @Synchronized
        fun getDatabase(context: Context): AppDatabase {
            if (databaseHelper == null) {
                databaseHelper = AppDatabaseHelper(context.applicationContext)
            }
            return databaseHelper!!.database
        }
    }

}