package com.formationandroid.kotlinmemo.metier.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.formationandroid.kotlinmemo.metier.dao.MemoDAO
import com.formationandroid.kotlinmemo.metier.dto.Memo

@Database(entities = [Memo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memoDAO(): MemoDAO
}
