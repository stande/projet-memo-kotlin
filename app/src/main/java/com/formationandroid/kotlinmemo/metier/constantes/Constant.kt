package com.formationandroid.kotlinmemo.metier.constantes

class Constant {
    companion object {
        const val PARAM_MEMO_DETAIL = "memo_param"
        const val FRAGMENT_MEMO_DETAIL = "memo_detail"
    }
}