package com.formationandroid.kotlinmemo.metier.dto

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "memo")
class Memo(
    var intitule: String
) {
    @PrimaryKey(autoGenerate = true)
    var memoId: Int = 0

    override fun toString(): String {
        return """
            Memo 
            
            ID : $memoId
            
            Nom : $intitule
            """.trimIndent()
    }
}