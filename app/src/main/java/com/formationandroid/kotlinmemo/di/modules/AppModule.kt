package com.formationandroid.kotlinmemo.di.modules

import android.app.Application
import android.content.Context
import com.formationandroid.kotlinmemo.metier.dao.MemoDAO
import com.formationandroid.kotlinmemo.metier.helpers.AppDatabaseHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    companion object {
        @Singleton
        @Provides
        fun provideContext(application: Application): Context {
            return application
        }

        @Singleton
        @Provides
        fun provideMemoDAO(context: Context): MemoDAO {
            return AppDatabaseHelper.getDatabase(context).memoDAO()
        }
    }

}