package com.formationandroid.kotlinmemo.di.components

import android.app.Application
import com.formationandroid.kotlinmemo.di.modules.AppModule
import com.formationandroid.kotlinmemo.metier.adapters.MemoAdapter
import com.formationandroid.kotlinmemo.models.repositories.MainRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    // Repositories :
    fun inject(mainRepository: MainRepository)
    fun inject(memoAdapter: MemoAdapter)
}