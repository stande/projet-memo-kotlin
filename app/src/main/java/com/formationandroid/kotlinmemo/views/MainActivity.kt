package com.formationandroid.kotlinmemo.views

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.formationandroid.kotlinmemo.R
import com.formationandroid.kotlinmemo.metier.adapters.MemoAdapter
import com.formationandroid.kotlinmemo.metier.dto.Memo
import com.formationandroid.kotlinmemo.metier.helpers.AppDatabaseHelper
import com.formationandroid.kotlinmemo.models.repositories.MainRepository
import com.formationandroid.kotlinmemo.viewModels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    // Injection main repository :
    @Inject
    @JvmField
    var mainRepository: MainRepository? = null

    // ViewModels :
    private lateinit var mainViewModel: MainViewModel

    // RecyclerView :
    private lateinit var recyclerView: RecyclerView

    // Adapter
    private lateinit var memoAdapter: MemoAdapter

    // Liste
    private lateinit var listMemos: MutableList<Memo>

    // Observer
    private lateinit var clickObserver: Observer<Memo>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Recycler View
        recyclerView = liste_memos
        // à ajouter pour de meilleures performances :
        recyclerView.setHasFixedSize(true)
        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        // Base de données
        // Declaration de la BDD + il faut au moins une requete pour initialiser la bdd (à l'init de la liste ici)
        AppDatabaseHelper.getDatabase(this)


        // ViewModel
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        mainViewModel.initViewModel(MainRepository())


        // liste Memos
        // val listMemos: MutableList<Memo> = AppDatabaseHelper.getDatabase(this).memoDAO().getAll()
        listMemos = mainViewModel.getAllMemos()

        // adapter :
        memoAdapter = MemoAdapter(listMemos, this)
        recyclerView.adapter = memoAdapter

        // TouchHelper
        val itemTouchHelper = ItemTouchHelper(
            ItemTouchHelperCallback(memoAdapter)
        )
        itemTouchHelper.attachToRecyclerView(recyclerView)

        clickObserver = Observer<Memo> { memo ->
            Log.d("TAG", "clickMemo : $memo")
            // Ajoute le memo dans la liste en 1ere position
            val newMemo = mainViewModel.getMemoByIntitule(memo.intitule)
            memoAdapter.addMemo(newMemo)
        }
        // Click Add Memo listener
        mainViewModel.liveDataItem!!.observe(this, clickObserver)
    }

    // On nettoie tout !!! Permet de fixer l'observateur qui se relance a la reouverture d'activite
    override fun onDestroy() {
        super.onDestroy()
        listMemos.clear()
        memoAdapter.notifyDataSetChanged();
        mainViewModel.liveDataItem!!.removeObserver(clickObserver)
        mainViewModel.liveDataItem = null
    }

    @Suppress("UNUSED_PARAMETER")
    fun clickButtonAddMemo(view: View?) {
        if (edit_text_intitule_memo.text.toString().isNotEmpty()) {
            val memo = Memo(edit_text_intitule_memo.text.toString())
            mainViewModel.clickButtonAddMemo(memo)
            edit_text_intitule_memo.text.clear()
        } else {
            Toast.makeText(
                this,
                "Vous devez saisir un intitulé avant de créer le mémo",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun deleteMemoByActivity(memo: Memo) {
        mainViewModel.postDeleteMemo(memo)
    }

    inner class ItemTouchHelperCallback(
        private var adapter: MemoAdapter
    ) : ItemTouchHelper.Callback() {

        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
            val dragFlagsUpDown = ItemTouchHelper.UP or ItemTouchHelper.DOWN
            val dragFlagsLeftRight = ItemTouchHelper.RIGHT
            return makeMovementFlags(
                dragFlagsUpDown,
                dragFlagsLeftRight
            )
        }

        override fun onMove(
            recyclerView: RecyclerView, viewHolder: ViewHolder,
            target: ViewHolder
        ): Boolean {
            adapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
            return true
        }

        override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
            if (direction == ItemTouchHelper.RIGHT) {
                this@MainActivity.deleteMemoByActivity(adapter.getMemoById(viewHolder.adapterPosition))
                adapter.onItemDismiss(viewHolder.adapterPosition)
            }
        }
    }


}