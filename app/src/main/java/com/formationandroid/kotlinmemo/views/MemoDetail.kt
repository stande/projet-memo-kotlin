package com.formationandroid.kotlinmemo.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.formationandroid.kotlinmemo.R
import com.formationandroid.kotlinmemo.metier.constantes.Constant
import kotlinx.android.synthetic.main.activity_memo_detail.*

class MemoDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memo_detail)
        val memoParam = intent.getStringExtra(Constant.PARAM_MEMO_DETAIL)
        intitule_memo_detail.text = memoParam
    }
}