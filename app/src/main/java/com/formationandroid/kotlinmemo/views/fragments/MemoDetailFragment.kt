package com.formationandroid.kotlinmemo.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.formationandroid.kotlinmemo.R
import com.formationandroid.kotlinmemo.metier.constantes.Constant
import kotlinx.android.synthetic.main.fragment_memo_detail.*

class MemoDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_memo_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (arguments != null && view != null) {
            val memoParam = arguments!!.getString(Constant.PARAM_MEMO_DETAIL)
            intitule_memo_detail.text = memoParam
        }
    }
}