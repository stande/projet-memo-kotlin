package com.formationandroid.kotlinmemo

import android.app.Application
import com.formationandroid.kotlinmemo.di.components.AppComponent
import com.formationandroid.kotlinmemo.di.components.DaggerAppComponent

class DIApplication : Application() {

    private var appComponent: AppComponent? = null

    override fun onCreate() {
        // initialisation :
        super.onCreate()
        instance = this
        // dagger :
        appComponent = DaggerAppComponent.builder().application(this).build()
    }

    companion object {
        private var instance: DIApplication? = null

        fun getAppComponent(): AppComponent? {
            return instance!!.appComponent
        }
    }
}