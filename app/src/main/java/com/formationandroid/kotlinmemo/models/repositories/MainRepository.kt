package com.formationandroid.kotlinmemo.models.repositories

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.formationandroid.kotlinmemo.DIApplication
import com.formationandroid.kotlinmemo.metier.dao.MemoDAO
import com.formationandroid.kotlinmemo.metier.dto.Memo
import javax.inject.Inject


class MainRepository {
    // Injections :
    @Inject
    @JvmField
    var memoDAO: MemoDAO? = null

    @Inject
    @JvmField
    var applicationContext: Context? = null

    init {
        DIApplication.getAppComponent()?.inject(this)
    }

    fun getAllMemos(): MutableList<Memo> {
        return memoDAO!!.getAll()
    }

    fun getMemoByIntitule(intitule: String): Memo {
        return memoDAO!!.findByIntitule(intitule)
    }

    fun postDeleteMemo(memo: Memo) {
        memoDAO!!.delete(memo)
    }

    fun postAddLiveDataMemo(mutableLiveDataItem: MutableLiveData<Memo>, memo: Memo) {
        memoDAO!!.insert(memo)
        mutableLiveDataItem.value = memo
    }
}