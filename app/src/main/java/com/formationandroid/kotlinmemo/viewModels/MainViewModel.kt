package com.formationandroid.kotlinmemo.viewModels

import androidx.lifecycle.*
import com.formationandroid.kotlinmemo.metier.dto.Memo
import com.formationandroid.kotlinmemo.models.repositories.MainRepository

class MainViewModel(
    private var mainRepository: MainRepository? = null,
    var liveDataItem: MutableLiveData<Memo>? = null
) : ViewModel() {

    // Initialisation :
    fun initViewModel(mainRepository: MainRepository?) {
        // vérification que l'initialisation n'a pas déjà été faite :
        if (liveDataItem != null) {
            return
        }
        // initialisation et premier chargement :
        this.mainRepository = mainRepository
        liveDataItem = MutableLiveData()
    }

    // Envoie liste de memos
    fun getAllMemos(): MutableList<Memo> {
        return mainRepository!!.getAllMemos()
    }

    fun getMemoByIntitule(intitule:String): Memo {
        return mainRepository!!.getMemoByIntitule(intitule)
    }

    // Supprimer un memo
    fun postDeleteMemo(memo: Memo) {
        mainRepository!!.postDeleteMemo(memo)
    }

    // Listener click bouton ajouter memo :
    fun clickButtonAddMemo(memo: Memo) {
        liveDataItem!!.let { mainRepository!!.postAddLiveDataMemo(it, memo) }
    }
}